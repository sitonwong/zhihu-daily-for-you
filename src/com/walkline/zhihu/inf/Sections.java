package com.walkline.zhihu.inf;

import java.util.Vector;

/**
 * 知乎专题分类集合
 * @author Walkline
 *
 */
public interface Sections extends com.walkline.zhihu.inf.Object
{
	public Vector getSections();
}