package com.walkline.zhihu.inf;

import java.util.Vector;

public interface DailyStories extends com.walkline.zhihu.inf.Object
{
	public String getDate();

	public Vector getStories();

	public Vector getTopStories();

	public boolean hasTopStories();
}