package com.walkline.zhihu.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.DailyStories;
import com.walkline.zhihu.inf.Story;

public class ZhihuDailyStories extends ZhihuObject implements DailyStories
{
	private String _date = "";
	private Vector _stories = new Vector();
	private Vector _top_stories = new Vector();

	public ZhihuDailyStories(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject dailyStories = jsonObject;
		if (dailyStories != null)
		{
			_date = dailyStories.optString("date");

			JSONArray storiesArray = dailyStories.optJSONArray("stories");
			if (storiesArray != null)
			{
				for (int i=0; i<storiesArray.length(); i++)
				{
					try {
						JSONObject storyObject = (JSONObject) storiesArray.get(i);

						Story story = new ZhihuStory(zhihu, storyObject);
						if (story != null) {_stories.addElement(story);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}

			JSONArray topStoriesArray = dailyStories.optJSONArray("top_stories");
			if (topStoriesArray != null)
			{
				for (int i=0; i<topStoriesArray.length(); i++)
				{
					try {
						JSONObject topStoriesObject = (JSONObject) topStoriesArray.get(i);

						Story topStories = new ZhihuStory(zhihu, topStoriesObject);
						if (topStories != null) {_top_stories.addElement(topStories);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public String getDate() {return _date;}

	public Vector getStories() {return _stories;}

	public Vector getTopStories() {return _top_stories;}

	public boolean hasTopStories() {return (_top_stories.size() >0);}
}