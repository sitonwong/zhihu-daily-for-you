package com.walkline.zhihu.inf;

/**
 * 知乎主题详情
 * @author Walkline
 */
public interface Theme
{
	public String getName();

	public String getDescription();

	public int getId();

	public String getImage();

	public int getColor();
}