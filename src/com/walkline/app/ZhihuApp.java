package com.walkline.app;

import net.rim.device.api.ui.UiApplication;

import com.walkline.screen.ZhihuScreen;

public class ZhihuApp extends UiApplication
{
    public static void main( String[] args )
    {
        ZhihuApp theApp = new ZhihuApp();
        theApp.enterEventDispatcher();
    }

    public ZhihuApp() {pushScreen(new ZhihuScreen());}
}