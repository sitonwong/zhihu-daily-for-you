package com.walkline.zhihu.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.Section;
import com.walkline.zhihu.inf.Sections;

public class ZhihuSections extends ZhihuObject implements Sections
{
	private Vector _sections = new Vector();

	public ZhihuSections(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject sections = jsonObject;
		if (sections != null)
		{
			JSONArray sectionsArray = sections.optJSONArray("data");
			if (sectionsArray != null)
			{
				for (int i=0; i<sectionsArray.length(); i++)
				{
					try {
						JSONObject sectionObject = (JSONObject) sectionsArray.get(i);

						Section section = new ZhihuSection(zhihu, sectionObject);
						if (section != null) {_sections.addElement(section);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public Vector getSections() {return _sections;}
}