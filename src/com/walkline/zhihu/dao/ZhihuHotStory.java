package com.walkline.zhihu.dao;

import org.json.me.JSONObject;

import com.walkline.app.ZhihuAppConfig;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.HotStory;

public class ZhihuHotStory extends ZhihuObject implements HotStory
{
	private String _title = "";
	private String _url = "";
	private String _thumbnail = "";
	private int _news_id = 0;

	public ZhihuHotStory(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject story = jsonObject;
		if (story != null)
		{
			_news_id = story.optInt("news_id");
			_title = story.optString("title");
			//_url = stories.optString("url");
			_thumbnail = story.optString("thumbnail");

			if (_news_id != 0) {_url = ZhihuAppConfig.queryStoryDetailsRequestURL + _news_id;}
		}
	}

	public String getTitle() {return _title;}

	public String getUrl() {return _url;}

	public String getThumbnail() {return _thumbnail;}

	public int getNewsId() {return _news_id;}
}